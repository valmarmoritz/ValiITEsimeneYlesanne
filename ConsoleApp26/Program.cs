﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp26
{
    /// <summary>
    ///
    /* Inimeste sünnipäevad: 

    Tee klass Inimene(Nimi, Sünniaeg)
    Tee list või massiiv kus Inimesed(min 20 tükki)
    Täida see ise väljamõeldud andmetega

    Seejärel loo programm, mis sellest nimekirjast leiab inimese kelle sünnipäev on järgmisena tulemas
    */
    /// </summary>
    class Program
    {
        static List<Inimene> inimesed = new List<Inimene>();
        static DateTime Vanim = DateTime.Parse("01.01.1950");
        static DateTime Noorim = DateTime.Parse("31.12.2000");

        static void Main(string[] args)
        {
            Algseis();
            // see osa lahendab ülesannet

            int i = 0;
            for (i=0; i < inimesed.Count; i++)
            Console.WriteLine($"{inimesed[i]} - {(DateTime.Now - inimesed[i].Sünniaeg).Duration().Days} päeva vana");

            int kaugus = int.MaxValue;
            Inimene järgmine = null;
            foreach (var kes in inimesed)
            {
                DateTime sp = kes.Sünniaeg;
                // sp = new DateTime(DateTime.Now.Year, sp.Month, sp.Day);    // see annab vea, kui inimene on sündinud liigaastal ja praegune aasta ei ole liigaasta

                sp = kes.Sünniaeg.AddDays(-1);
                sp = new DateTime(DateTime.Now.Year, sp.Month, sp.Day).AddDays(1);
                if (sp < DateTime.Now.Date) sp = sp.AddYears(1);

                if ((sp - DateTime.Now.Date).Days < kaugus)
                {
                    järgmine = kes;
                    kaugus = (sp - DateTime.Now.Date).Days;
                }
            }

            Console.WriteLine($"Kõige lähem sünnipäev on inimesel {järgmine}, tema sünnipäevani on {kaugus} päeva.");
        }

        static void Algseis()
        {
            // see osa valmistab andmed ette

            string[] nimed = { "Pille", "Malle", "Moonika", "Jaan", "Juhan", "Indrek", "Mati", "Uku", "Hans", "Peeter",
                "Joosep", "Katrin", "Helena", "Julija", "Jaanika", "Külli", "Jüri", "Mari", "Pearu", "Johannes", "Karl" };
            int päevi = (Noorim - Vanim).Days;
            Random suvalineArv = new Random();

            foreach (var x in nimed)
            {
                inimesed.Add(new Inimene(x, Vanim.AddDays(suvalineArv.Next() % päevi)));
            }

            inimesed.Add(new Inimene("Kahtlane", DateTime.Parse("29.02.2000")));
        }
    }

    class Inimene
    {
        public string Nimi;
        public DateTime Sünniaeg;

        public Inimene(string nimi, DateTime sünniaeg)
        {
            this.Nimi = nimi;
            this.Sünniaeg = sünniaeg;
        }

        public override string ToString()
        {
            return $"{Nimi}, sündinud {Sünniaeg.Day}.{Sünniaeg.Month}.{Sünniaeg.Year}";
        }
    }
    }
